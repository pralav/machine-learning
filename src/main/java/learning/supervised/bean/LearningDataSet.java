package learning.supervised.bean;

import learning.unsupervised.clustering.bean.Pair;

import java.util.ArrayList;
import java.util.Arrays;

/**
 * User: prashanth.v
 * Date: 7/3/13
 * Time: 8:14 PM
 */
public class LearningDataSet {
    ArrayList<LearningDataPair> dataSet;
    double[][] inputs;
    double[][] outputs;
    Double[] max;
    Double[] min;
    int numAttributes;
    int outputSize;


    public LearningDataSet(ArrayList<LearningDataPair> dataSet) {
        this.dataSet = dataSet;
        System.out.println(dataSet.size());
        Pair<Double[], Double[]> dataPair = dataSet.size()>0 && dataSet.get(0) != null ?  dataSet.get(0).getDataPair():null;
        if (dataPair != null)
            numAttributes = dataPair.getFirst() == null ? 0 : dataPair.getFirst().length;
        if (dataPair != null)
            outputSize = dataPair.getSecond() == null ? 0 : dataPair.getSecond().length;
        max = new Double[numAttributes];
        min = new Double[numAttributes];
        Arrays.fill(max, Double.MIN_VALUE);
        Arrays.fill(min, Double.MAX_VALUE);
        for (LearningDataPair dataPair1 : dataSet) {
            updateMinMax(dataPair1.getInput());
        }
    }

    public LearningDataSet(double[][] inputs, double[][] outputs) throws Exception {
        this.numAttributes = inputs.length == 0 ? 0 : inputs[0].length;
        this.outputSize = outputs.length == 0 ? 0 : outputs[0].length;
        this.dataSet = new ArrayList<LearningDataPair>();
        max = new Double[numAttributes];
        min = new Double[numAttributes];
        Arrays.fill(max, Double.MIN_VALUE);
        Arrays.fill(min, Double.MAX_VALUE);
        if (inputs.length != outputs.length) {
            throw new Exception("Input and Output length mismatch");
        }
        int i = 0;
        for (double[] input : inputs) {
            double[] outputList = new double[outputs[0].length];
            System.arraycopy(outputs[i], 0, outputList, 0, outputList.length);
            System.out.println(outputList.length);
            updateMinMax(input);
            this.dataSet.add(new LearningDataPair(input, outputList));
            i++;
        }
    }

    public void updateMinMax(double[] input) {
        for (int i = 0; i < input.length; i++) {
            if (min[i] > input[i])
                min[i] = input[i];
            if (max[i] < input[i])
                max[i] = input[i];
        }
    }

    public void updateMinMax(Double[] input) {
        for (int i = 0; i < input.length; i++) {
            if (min[i] > input[i])
                min[i] = input[i];
            if (max[i] < input[i])
                max[i] = input[i];
        }
    }

    public ArrayList<LearningDataPair> getDataSet() {
        return dataSet;
    }

    public void setDataSet(ArrayList<LearningDataPair> dataSet) {
        this.dataSet = dataSet;
    }

    public Double[] getMax() {
        return max;
    }

    public void setMax(Double[] max) {
        this.max = max;
    }

    public Double[] getMin() {
        return min;
    }

    public void setMin(Double[] min) {
        this.min = min;
    }

    public int getNumAttributes() {
        return numAttributes;
    }

    public void setNumAttributes(int numAttributes) {
        this.numAttributes = numAttributes;
    }

    public int getSize() {
        return dataSet.size();
    }

    public int getOutputSize() {
        return outputSize;
    }

    public void normalizeData() {
        for (LearningDataPair dataPair : dataSet) {
            normalizeDataPair(dataPair);
        }
    }

    private void normalizeDataPair(LearningDataPair dataPair) {
        dataPair.normalizeData(max, min);
    }
}
