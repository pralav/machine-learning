package learning.supervised.classification.Knn;

import learning.supervised.bean.LearningDataPair;
import learning.supervised.bean.LearningDataSet;

import java.util.ArrayList;
import java.util.Arrays;

/**
 * Created with IntelliJ IDEA.
 * User: prashanth.v
 * Date: 7/5/13
 * Time: 11:59 AM
 * To change this template use File | Settings | File Templates.
 */
public class DiscreteKnnClassifier extends KnnClassifier {
    int noCategories;

    public DiscreteKnnClassifier(int k, LearningDataSet trainingSet, Distance distanceFunction, int noCategories,boolean isNormalized) {
        super(k, trainingSet, distanceFunction,isNormalized);
        this.noCategories = noCategories;
        if(!isNormalized){
            trainingSet.normalizeData();
        }
    }

    @Override
    public double classify(LearningDataPair newLearningDataPair) {

        ArrayList<LearningDataPair> nearestNeighbours = getNearestNeighbours(newLearningDataPair);

        double[] weights = new double[noCategories];
        Arrays.fill(weights, 0);
        for (LearningDataPair neighbor : nearestNeighbours) {
            int label = (int) Math.floor(neighbor.getIdeal(0));
            weights[label] += getWeight(distanceFunction.getDistance(neighbor, newLearningDataPair));
        }
        double max = 0;
        int max_label = -1;
        for (int i = 0; i < noCategories; i++) {
            if (Double.isNaN(weights[i])) {
                max_label = i;
                break;
            }
            if (weights[i] > max) {
                max = weights[i];
                max_label = i;
            }
        }

        return max_label;
    }

}
