package learning.supervised.classification.databean;

import learning.supervised.bean.LearningDataPair;
import learning.supervised.bean.LearningDataSet;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created with IntelliJ IDEA.
 * User: prashanth.v
 * Date: 7/6/13
 * Time: 9:10 PM
 * To change this template use File | Settings | File Templates.
 */
public class ClassificationDataSet extends LearningDataSet {
    int numClasses;
    ArrayList<Attribute> attributes = new ArrayList<Attribute>();
   // publ


    public ClassificationDataSet(ArrayList<LearningDataPair> dataPairs,int numClasses) {
        super(dataPairs);
        assignAttributes(dataPairs);
        this.numClasses=numClasses;
    }


    public ClassificationDataSet(double[][] inputs, double[][] outputs, int classes) throws Exception {
        super(inputs, outputs);
        assignAttributes(inputs);
        this.numClasses = classes;
    }

    private void assignAttributes(double[][] inputs) {
        ArrayList<Attribute> attributes=new ArrayList<Attribute>();
        HashMap<Double, Integer> uniqueAttributes;
        for (int i = 0; i < inputs[0].length; i++) {
            Double[] data = new Double[inputs.length];
            uniqueAttributes = new HashMap<Double, Integer>();
            for (int j = 0; j < inputs.length; j++) {
                data[j]=inputs[j][i];
                int count = uniqueAttributes.get(inputs[j][i]) == null ? 0 : uniqueAttributes.get(inputs[j][i]) + 1;
                uniqueAttributes.put(inputs[j][i], count);
            }
            attributes.add(new Attribute(data, uniqueAttributes, i));
        }
        setAttributes(attributes);
    }

    private void assignAttributes(ArrayList<LearningDataPair> inputs) {
        ArrayList<Attribute> attributes=new ArrayList<Attribute>();
        HashMap<Double, Integer> uniqueAttributes;
        for (int i = 0;  inputs.size()>0 && i< inputs.get(0).getNumAttributes(); i++) {
            Double[] data = new Double[inputs.size()];
            uniqueAttributes = new HashMap<Double, Integer>();
            int j=0;
            for (LearningDataPair input : inputs) {
                data[j++]=input.value(i);
                int count = uniqueAttributes.get(input.value(i)) == null ? 0 : uniqueAttributes.get(input.value(i)) + 1;
                uniqueAttributes.put(input.value(i), count);
            }
            attributes.add(new Attribute(data, uniqueAttributes, i));
        }
        setAttributes(attributes);
    }

    public Attribute getAttribute(int i) {
        if (i >= attributes.size()) return null;
        return attributes.get(i);
    }

    public int getNumClasses() {
        return numClasses;
    }

    public void setNumClasses(int numClasses) {
        this.numClasses = numClasses;
    }

    public ArrayList<Attribute> getAttributes() {
        return attributes;
    }

    public void setAttributes(ArrayList<Attribute> attributes) {
        this.attributes = attributes;
    }
}
