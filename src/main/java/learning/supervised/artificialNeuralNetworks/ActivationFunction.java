package learning.supervised.artificialNeuralNetworks;

/**
 * Created with IntelliJ IDEA.
 * User: prashanth.v
 * Date: 7/3/13
 * Time: 2:02 PM
 * To change this template use File | Settings | File Templates.
 */
public interface ActivationFunction {
    public double activate(double sum);
    public double partialDerivative(double a,double b);
}
