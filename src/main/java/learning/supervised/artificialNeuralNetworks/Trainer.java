package learning.supervised.artificialNeuralNetworks;

import learning.supervised.artificialNeuralNetworks.algorithms.BackPropogation;
import learning.supervised.artificialNeuralNetworks.algorithms.Propogation;
import learning.supervised.bean.LearningDataPair;
import learning.supervised.bean.LearningDataSet;

/**
 * Created with IntelliJ IDEA.
 * User: prashanth.v
 * Date: 7/3/13
 * Time: 8:08 PM
 * To change this template use File | Settings | File Templates.
 */
public class Trainer {
    ANN network;
    LearningDataSet dataSet;

    public Trainer(ANN network, LearningDataSet dataSet) {
        this.dataSet = dataSet;
        this.network = network;
    }

    public void train(int count) {
        for (int i = 0; i < count; i++) {
            for (LearningDataPair pair : dataSet.getDataSet()) {
                network.setDataPair(pair);
                Propogation prop=new BackPropogation(network,dataSet,0.03,0.7);
                prop.iteration(1);
            }
        }

    }
}
