package learning.unsupervised.clustering.bean;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created with IntelliJ IDEA.
 * User: prashanth.v
 * Date: 6/20/13
 * Time: 3:01 PM
 * To change this template use File | Settings | File Templates.
 */
public class ClusterData {
    int clusterCount;
    ArrayList<ClusterDataPoint> dataSet;
    HashMap<Integer,ClusterCenters> clusterCenters;

    public ClusterData(Double[][] dataSet, int clusterCount) {
        this.clusterCount=clusterCount;
        this.dataSet=new ArrayList<ClusterDataPoint>();
        for (Double[] data : dataSet) {
            this.dataSet.add(new ClusterDataPoint(data));
        }
        this.clusterCenters=new HashMap<Integer, ClusterCenters>(clusterCount);//ClusterCenters[clusterCount];
    }
    public void addClusterCenter(ClusterCenters center){
        clusterCenters.put(center.getId(), center);
    }

    public ArrayList<ClusterDataPoint> getDataSet() {
        return dataSet;
    }

    public void setDataSet(ArrayList<ClusterDataPoint> dataSet) {
        this.dataSet = dataSet;
    }

    public ClusterData(ArrayList<ClusterDataPoint> dataSet, int clusterCount) {
        this.dataSet = dataSet;
        this.clusterCount = clusterCount;
        clusterCenters = new HashMap<Integer, ClusterCenters>(clusterCount);//ClusterCenters[clusterCount];
    }

    public ClusterData(ArrayList<ClusterDataPoint> dataSet) {
        this.dataSet = dataSet;
        this.clusterCount = 0;
        clusterCenters = new HashMap<Integer, ClusterCenters>(clusterCount);//ClusterCenters[clusterCount];
    }

    public int getClusterCount() {
        return clusterCount;
    }

    public void setClusterCount(int clusterCount) {
        this.clusterCount = clusterCount;
    }

  /*  public ClusterCenters[] getClusterCenters() {
        return clusterCenters;
    }

    public void setClusterCenters(ClusterCenters[] clusterCenters) {
        this.clusterCenters = clusterCenters;
    }*/
  public HashMap<Integer,ClusterCenters> getClusterCenters() {
        return clusterCenters;
    }

    public void setClusterCenters(HashMap<Integer,ClusterCenters> clusterCenters) {
        this.clusterCenters = clusterCenters;
    }
}
