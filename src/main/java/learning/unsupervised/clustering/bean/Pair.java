package learning.unsupervised.clustering.bean;

import java.util.TreeMap;

/**
 * Created with IntelliJ IDEA.
 * User: prashanth.v
 * Date: 4/22/13
 * Time: 6:36 PM
 * To change this template use File | Settings | File Templates.
 */
public class Pair<E, T> implements Comparable<Pair<E, T>> {
    E first;
    T second;
    public  Pair(){
        first=null;
        second=null;
    }

    public Pair(E first, T second) {
        this.first = first;
        this.second = second;
    }

    public E getFirst() {
        return first;
    }

    public void setFirst(E first) {
        this.first = first;
    }

    public T getSecond() {
        return second;
    }

    public void setSecond(T second) {
        this.second = second;
    }

    @Override
    public String toString() {
        return "<" +
                first.toString() +
                ", " + second.toString() +
                '>';
    }

    public int compareTo(Pair<E, T> o) {
        if(second.equals(o.second)) return 0;
        return ((Comparable) second).compareTo((Comparable) o.second);
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (obj == null || obj.getClass() != this.getClass()) {
            return false;
        }
        Pair guest = (Pair) obj;
        return first.equals(guest.first);
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result
                + ((first == null) ? 0 : first.hashCode());

        return result;
    }

    public static void main(String[] args) {
        TreeMap<Pair<Integer, Double>, Double> map = new TreeMap<Pair<Integer, Double>, Double>();
        map.put(new Pair<Integer, Double>(1, 3.0), 5.0);
        map.put(new Pair<Integer, Double>(2, 4.0), 5.0);
        map.put(new Pair<Integer, Double>(4, 2.0), 5.0);
        System.out.println(map.firstKey().getFirst());
        System.out.println(map.get(new Pair<Integer, Double>(map.firstKey().first, 0.0)));
    }


}
