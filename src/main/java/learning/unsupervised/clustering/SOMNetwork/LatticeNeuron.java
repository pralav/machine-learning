package learning.unsupervised.clustering.SOMNetwork;

import java.util.ArrayList;
import java.util.Random;

/**
 * Created with IntelliJ IDEA.
 * User: pvijayaraghavan
 * Date: 7/19/13
 * Time: 6:08 PM
 * To change this template use File | Settings | File Templates.
 */
public class LatticeNeuron {
    double  x,y;
    ArrayList<Double> weights;
    int featureLength;
    Random random=new Random();

    public LatticeNeuron(double x, double y,int featureLength) {
        this.x = x;
        this.y = y;
        this.featureLength=featureLength;
        weights=new ArrayList<Double>();
        for(int i=0;i<featureLength;i++)
            weights.add(random.nextDouble());
    }
    public double calculateDistance(Double[] input){
        double distance=0;
        for(int i=0;i<input.length;i++){
          distance+=Math.pow(input[(i)]-weights.get(i),2);
        }
        return distance;
    }
    public void alterWeight(Double[] ideal,double learningRate,double distanceDecayingInfluence){
       for(int i=0;i<weights.size();i++){
           double newWeight=weights.get(i)+learningRate*distanceDecayingInfluence*(ideal[(i)]-weights.get(i));
           weights.set(i,newWeight);

       }
    }

    public double getX() {
        return x;
    }

    public void setX(double x) {
        this.x = x;
    }

    public double getY() {
        return y;
    }

    public void setY(double y) {
        this.y = y;
    }

    public ArrayList<Double> getWeights() {
        return weights;
    }

    public void setWeights(ArrayList<Double> weights) {
        this.weights = weights;
    }

    public int getFeatureLength() {
        return featureLength;
    }

    public void setFeatureLength(int featureLength) {
        this.featureLength = featureLength;
    }

    public Random getRandom() {
        return random;
    }

    public void setRandom(Random random) {
        this.random = random;
    }
}
