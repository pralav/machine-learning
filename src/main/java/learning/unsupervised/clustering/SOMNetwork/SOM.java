package learning.unsupervised.clustering.SOMNetwork;

import java.util.ArrayList;
import java.util.Random;

/**
 * Created with IntelliJ IDEA.
 * User: pvijayaraghavan
 * Date: 7/19/13
 * Time: 12:36 AM
 * To change this template use File | Settings | File Templates.
 */
public class SOM {
    int latticeWidth;
    int latticeHeight;
    int iterations;
    LatticeNeuron[][] neurons;
    double radius, timeConstant, learningRate;
    int inputVectorSize;
    boolean done;
    double decayingInfluence;
    double learningRate0 = 0.1;

    public SOM(int latticeWidth, int latticeHeight, int iterations, int inputSize) {
        this.latticeWidth = latticeWidth;
        this.latticeHeight = latticeHeight;
        this.iterations = iterations;
        neurons = new LatticeNeuron[latticeWidth][latticeHeight];
        radius = Math.max(latticeWidth, latticeHeight) / 2;
        timeConstant = iterations / Math.log(radius);
        this.inputVectorSize = inputSize;
        done = false;
    }


    boolean epoch(ArrayList<Double[]> data, int currentCount) {
        Random random = new Random();
        for (Double[] input : data) {
            if (input.length != inputVectorSize) return false;
            if (done) return true;

            int idx = random.nextInt(data.size() - 1);
            LatticeNeuron winner = findBMU(data.get(idx));
            double neighbourRadius = radius * Math.exp(-(double) iterations / timeConstant);
            for (int i = 0; i < latticeWidth; i++) {
                for (int j = 0; j < latticeHeight; j++) {
                    double distanceSquare = (winner.getX() - neurons[i][j].getX()) *
                            (winner.getX() - neurons[i][j].getX()) +
                            (winner.getY() - neurons[i][j].getY()) *
                                    (winner.getY() - neurons[i][j].getY());

                    double radiusSquare = neighbourRadius * neighbourRadius;

                    if (distanceSquare < (neighbourRadius * neighbourRadius)) {
                        decayingInfluence = Math.exp(-(distanceSquare) / (2 * radiusSquare));
                        neurons[i][j].alterWeight(data.get(idx),
                                learningRate,
                                decayingInfluence);
                    }
                }
                learningRate = learningRate0 * Math.exp(-(double) currentCount / iterations);

            }
        }

        return true;
    }

    public LatticeNeuron findBMU(Double[] input) {
        LatticeNeuron winner = null;
        double minDist = Double.MAX_VALUE;
        for (int i = 0; i < latticeWidth; i++) {
            for (int j = 0; j < latticeHeight; j++) {
                double dist = neurons[i][j].calculateDistance(input);
                if (dist < minDist) {
                    minDist = dist;
                    winner = neurons[i][j];
                }
            }
        }
        return winner;
    }
}
