package learning.unsupervised.clustering.Algorithms;

import learning.unsupervised.clustering.bean.ClusterCenters;
import learning.unsupervised.clustering.bean.ClusterData;
import learning.unsupervised.clustering.bean.ClusterDataPoint;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 * User: prashanth.v
 * Date: 6/19/13
 * Time: 5:04 PM
 * To change this template use File | Settings | File Templates.
 */
public class DBScan {
    ClusterData clusterData;
    double eps;
    int minPts;
    int clusterCount;


    public DBScan(ClusterData clusterData, int minPts, double eps) {
        this.clusterData = clusterData;
        this.minPts = minPts;
        this.eps = eps;
        clusterCount = -1;
    }

    public void clusterData() {
        HashSet<ClusterDataPoint> neighbourPoints = new HashSet<ClusterDataPoint>();
        ArrayList<ClusterDataPoint> dataSet = clusterData.getDataSet();
        for (ClusterDataPoint dataPoint : dataSet) {
            if (!dataPoint.isVisited()) {
                dataPoint.setVisited(true);
                neighbourPoints = collectNeighbours(dataPoint);
                if (neighbourPoints.size() < minPts) {
                    dataPoint.setNoise(true);
                } else {
                    clusterCount++;
                    expandCluster(dataPoint, neighbourPoints);
                }

            }
        }

    }

    private void expandCluster(ClusterDataPoint dataPoint, HashSet<ClusterDataPoint> neighbourPoints) {
        ClusterCenters clusterCenters = new ClusterCenters(dataPoint, clusterCount);
        clusterCenters.addMemberPoint(dataPoint);
        for (ClusterDataPoint neighbour : neighbourPoints) {
            HashSet<ClusterDataPoint> neighboursNeighbour;
            if (!neighbour.isVisited()) {
                neighbour.setVisited(true);
                neighboursNeighbour = collectNeighbours(neighbour);
                if (neighboursNeighbour.size() >= minPts) {
                    neighboursNeighbour.addAll(neighbourPoints);
                    neighbourPoints=neighboursNeighbour;
                }
            }
            if (neighbour.getAssociatedCluster() == -1) {
                neighbour.setAssociatedCluster(clusterCount);
                clusterCenters.addMemberPoint(neighbour);
            }
        }
        clusterData.addClusterCenter(clusterCenters);
    }

    public double calculateDistance(Double[] a, Double[] b) {
        if (a.length != b.length) {
            throw new IllegalArgumentException("The dimensions have to be equal!");
        }
        double sum = 0.0;
        for (int i = 0; i < a.length; i++) {
            sum += Math.pow(a[i] - b[i], 2);
        }
        return Math.sqrt(sum);
    }


    private HashSet<ClusterDataPoint> collectNeighbours(ClusterDataPoint dataPoint) {
        HashSet<ClusterDataPoint> neighbours = new HashSet<ClusterDataPoint>();
        for (ClusterDataPoint dataPoint1 : clusterData.getDataSet()) {
            if (calculateDistance(dataPoint1.getDataValues(), dataPoint.getDataValues()) <= eps) {
                neighbours.add(dataPoint1);
            }
        }
        return neighbours;
    }

    public static void main(String[] args) {
        ArrayList<ClusterDataPoint> dataSet = new ArrayList<ClusterDataPoint>();
        Double[][] datas = new Double[][]{{2.0, 3.5}, {3.0, 5.0}, {6.0, 7.0}, {8.0, 9.0}, {1.0, 5.0}, {2.0, 5.0}, {3.0, 6.0}, {9.5, 2.0}, {0.0, 1.0}, {1.0, 2.0}, {3.0, 1.0}};
        DBScan clustering = new DBScan(new ClusterData(datas, 2), 2, 2);
        clustering.clusterData();
        for (Map.Entry<Integer, ClusterCenters> center : (clustering.clusterData.getClusterCenters().entrySet())) {
            System.out.println(center.getValue().getMemberPoints().toString());
        }
    }


}
