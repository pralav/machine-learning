package learning.unsupervised.clustering.Algorithms;

import learning.unsupervised.clustering.bean.Similarity;

import java.util.Comparator;
import java.util.Map;
import java.util.PriorityQueue;

/**
 * Created with IntelliJ IDEA.
 * User: prashanth.v
 * Date: 7/2/13
 * Time: 6:21 PM
 * To change this template use File | Settings | File Templates.
 */
public class ValueComparator implements Comparator<Integer> {

    Map<Integer, PriorityQueue<Similarity>> base;
    public ValueComparator(Map<Integer, PriorityQueue<Similarity>> base) {
        this.base = base;
    }

    public int compare(Integer a, Integer b) {
        PriorityQueue<Similarity> c1=base.get(a);
        PriorityQueue<Similarity> c2=base.get(b);
        if(c1==null || c2==null) return 0;

        Similarity s1=c1.peek();
        Similarity s2=c2.peek();
        if(s1==null || s2==null) return 0;
        if (s1.getSim() >= s2.getSim()) {
            return -1;
        } else {
            return 1;
        }
    }
}
